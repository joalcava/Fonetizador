# -*- coding: utf-8 -*-

"""Usage:
    fonetizador.py -m FILE ... [-o --override]
    fonetizador.py (-p | --process) WORD
    fonetizador.py FILE OUTFILE
    fonetizador.py FILE [-o --override]
    fonetizador.py (-h | --help)
    fonetizador.py (-v | --version)

Realiza fonetizacion de una palabra, procesa una cadena, un archivo o un lote de archivos.

Arguments:
    FILE        Archivo de entrada
    FILE ...    Multiples archivos de entrada
    WORD        Cadena para ser procesada directamente
    OUTFILE     Archivo de salida

Options:
    -h --help       Muestra esta pantalla.
    -v --version    Muestra la version.
    -o --override   Sobreescribe el archivo(s) de entrada.
    -m              Indica que se van a procesar multiples archivos.
    -d --divide     Indica que se va a realizar la division de una palabra
"""

__version__ = '1.0.1'

import re
from collections import OrderedDict
from syllab_divisor import SyllabDivisor
from word import Word
from docopt import docopt


class Fonetizador:
    """Fonetizador
    """

    def __init__(self):
        self.__digrafos = ['cc', 'ch', 'll', 'rr']
        self.reglas = {

            'a':
            OrderedDict([
                (r"a", 'a'),
                (r"a[ouóú]", 'A'),
                (r"na", 'Na'),
                (r"ma[mn]", 'Na'),
            ]),

            'á': OrderedDict([
                (r"á", 'Ta'),
                (r"ná", 'Tna'),
                (r"má[mn]", 'Tna'),
                (r"[gkjqc]á", 'TA'),
            ]),

            'e': OrderedDict([
                (r"e", 'e'),
                (r"ne", 'Ne'),
                (r"me[mn]", 'Ne'),
            ]),

            'é': OrderedDict([
                (r"é", 'Te'),
                (r"né", 'Tne'),
                (r"mé[mn]", 'Tne'),
                (r"[gkjq]é", 'TE'),
            ]),

            'i': OrderedDict([
                (r"i", 'i'),
                (r"i[aeouáéóú]", 'j'),
                (r"ni", 'Ni'),
                (r"mi[mn]", 'Ni'),
            ]),

            'í': OrderedDict([
                (r"í", 'Ti'),
                (r"í[aeouáéóú]", 'Tj'),
                (r"ní", 'Tni'),
                (r"ní[aeouáéóú]", 'Tnj'),
                (r"mí[mn]", 'Tni'),
                (r"[gkjq]í", 'TI'),
            ]),

            'o': OrderedDict([
                (r"o", 'o'),
                (r"no", 'No'),
                (r"mo[mn]", 'No'),
            ]),

            'ó': OrderedDict([
                (r"ó", 'To'),
                (r"nó", 'Tno'),
                (r"mó[mn]", 'Tno'),
                (r"[gkjqc]ó", 'TO'),
            ]),

            'u': OrderedDict([
                (r"u", 'u'),
                (r"u[aieoáíéó]", 'w'),
                (r"nu", 'Nu'),
                (r"mu[mn]", 'Nu'),
                (r"qu", ''),
            ]),

            'ü': OrderedDict([
                (r"ü", 'u'),
                (r"ü[aieoáíéó]", 'w'),
                (r"nü", 'Nu'),
                (r"mü[mn]", 'Nu'),
                (r"qü", ''),
            ]),

            'ú': OrderedDict([
                (r"ú", 'Tu'),
                (r"nú", 'Tnu'),
                (r"mú[mn]", 'Tnu'),
                (r"[gkjqc]ú", 'TU'),
            ]),

            'b': OrderedDict([
                (r"[aeiouáéíóú]b[aeiouáéíóú]", 'B'),
                (r"^b", 'b'),
                (r"b$", 'b'),
                (r"[cdfghjklmnpqrstvwxz]b[áéíóúaeiou]", 'b'),
                (r"[áéíóúaeiou]b[cdfghjklmnpqrstvwxz]", 'b'),
            ]),

            'c': OrderedDict([
                (r"c[áóúaoubdfgjklmnpqrstvwxz]", 'k'),
                (r"c$", 'k'),
                (r"[^c]c[eéíi]", 's'),
                (r"^c[eéíi]", 's'),
            ]),

            'cc': {r"cc": 'ks'},

            'ch': {r"ch": 'c'},

            'd': OrderedDict([
                (r"[aeiouáéíóú]d[aeiouáéíóú]", 'D'),
                (r"[aeiouáéíóú]d[aeiouáéíóú]$", 'D'),
                (r"[bcfghjklmnpqrstvwxz]d[aeiouáéíóú]", 'd'),
                (r"^d", 'd'),
                (r"d$", 'd'),
                (r"[aeiouáéíóú]d[bcfghjklmnpqrstvwxz]", 'd'),
            ]),

            'f': {r"f": 'f'},

            'g': {r"g": 'g'},

            'j': {r"j": 'x'},

            'k': {r"k": 'k'},

            'l': OrderedDict([
                (r"[^l]l", 'l'),
                (r"^l[^l]", 'l'),
                (r"ll", 'j'),
            ]),

            'll': {r"ll": 'j'},

            'm': {r"m": 'm'},

            'n': OrderedDict([
                (r"n[áéíóúaeioucdghjklpqrstwxz]", 'n'),
                (r"[áéíóúaeioubcdghjklpqrstvwxzmf]n[^vbmf]", 'n'),
                (r"n$", 'n'),
                (r"n[vbmf]", 'm'),
            ]),

            'ñ': {r"ñ": 'n.'},

            'p': {r"p": 'p'},

            'q': {r"q": 'k'},

            'r': OrderedDict([
                (r"[^r]r", 'r'),
                (r"^r", 'R'),
                (r"r$", 'R'),
                (r"[aeiouáéíóú]r[bcdfghjklmnpqstvwxz]", 'R'),
                (r"[bcdfghjklmnpqstvwxz]r[aeiouáéíóú]", 'R'),
                (r"rr", 'R'),
            ]),

            'rr': {r"rr": 'R'},

            's': {r"s": 's'},

            't': {r"t": 't'},

            'v': OrderedDict([
                (r"^v", 'b'),
                (r"v$", 'b'),
                (r"[bcdfghjklmnpqrstvwxz]v[aeiouáéíóú]", 'b'),
                (r"[aeiouáéíóú]v[bcdfghjklmnpqrstvwxz]", 'b'),
                (r"[^aeiouáéíóú]v[^aeiouáéíóú]", 'b'),
                (r"[aeiouáéíóú]v[aeiouáéíóú]", 'B'),
            ]),

            'w': OrderedDict([
                (r"w[iaeo]", 'w'),
                (r"w(?![iaeo])[ubcdfghjklmnpqrstvwxz]*", 'u'),
            ]),

            'x': {r"x": 'ks'},

            'y': {r"y": 'j'},

            'z': {r"z": 's'},
        }

    def procesar(self, word):
        """Fonetiza una palabra
        """
        if not word.divided:
            sdivisor = SyllabDivisor()
            sdivisor.divide(word)
        palabra = self.marcar_acento(word)
        palabra = self.eliminar_h(palabra)
        longitud = len(palabra)
        fonemas = ['' for i in range(longitud)]
        for letra in self.reglas:
            for regla in self.reglas[letra]:
                resultados = re.finditer(regla, palabra)
                if not resultados is None:
                    for resultado in resultados:
                        posicion_relativa = resultado.group().find(letra)
                        posicion_abosluta = resultado.start() + posicion_relativa
                        fonemas[posicion_abosluta] = self.reglas[letra][regla]
        word.phonemes = [fonema for fonema in fonemas if fonema != '']

    def marcar_acento(self, word):
        pos = word.accenttype.value * -1
        wd = word.syllables
        vocales = {'a':'á', 'e': 'é', 'i': 'í', 'o': 'ó', 'u': 'ú', 'á':'á', 'é': 'é', 'í': 'í', 'ó': 'ó', 'ú': 'ú'}
        for letter in reversed(wd[pos]):
            if letter in vocales:
                wd[pos] = wd[pos].replace(letter, vocales[letter])
                break
        return ''.join(wd)


    def eliminar_h(self, s_word):
        """Eliminando la letra /h/ de una palabra a menos que esta conforme una /ch/
        params:
            s_word (string): Palabra que se va a procesar
        return:
            string
        """
        # Se elimina la /h/ al inicio si existe
        if s_word[0] == 'h':
            s_word = s_word[1:len(s_word)]
        # Se extraen las letras /h/ con una expresion regular cuando no conforman /ch/
        h_sin_c = re.compile('[^c]h')
        result = h_sin_c.finditer(s_word)
        if not result is None:
            for match in result:
                s_word = s_word[0 : match.span()[1]-1] + s_word[match.span()[1]:len(s_word)]
        return s_word


if __name__ == '__main__':
    ARGUMENTS = docopt(__doc__, version=__version__)
    FONETIZADOR = Fonetizador()

    if ARGUMENTS['--process']:
        WORD = Word(ARGUMENTS['WORD'])
        FONETIZADOR.procesar(WORD)
        print(WORD.phonemes)
    else:
        for file in ARGUMENTS['FILE']:
            try:
                word_list = open(file, 'r', encoding='utf8').read()
            except UnicodeDecodeError:
                word_list = open(file, 'r').read()
            word_list = word_list.split()
            word_list = [Word(word) for word in word_list]
            for word in word_list:
                FONETIZADOR.procesar(word)
            textfile = [word.phonemes_str() for word in word_list]
            textfile = '\n'.join(textfile)
            if ARGUMENTS['--override']:
                file = open(file, 'w', encoding='utf8')
            elif ARGUMENTS['OUTFILE']:
                file = open(ARGUMENTS['OUTFILE'], 'w', encoding='utf8')
            else:
                file = open('FONETIZADO' + file, 'w', encoding='utf8')
            file.write(textfile)
