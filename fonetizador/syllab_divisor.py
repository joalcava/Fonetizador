# -*- coding: utf-8 -*-

"""Usage:
    syllab_divisor.py -m FILE ... [-o --override]
    syllab_divisor.py (-p | --process) WORD
    syllab_divisor.py FILE OUTFILE
    syllab_divisor.py FILE [-o --override]
    syllab_divisor.py (-h | --help)
    syllab_divisor.py (-v | --version)

Realiza la division silabica basado en las reglas de division silabica del idioma español.

Arguments:
    FILE        Archivo de entrada
    FILE ...    Multiples archivos de entrada
    WORD      Cadena para ser procesada directamente
    OUTFILE     Archivo de salida

Options:
    -h --help       Muestra esta pantalla.
    -v --version    Muestra la version.
    -o --override   Sobreescribe el archivo(s) de entrada.
    -m              Indica que se van a procesar multiples archivos.
    -d --divide     Indica que se va a realizar la division de una palabra
"""

__version__ = '1.0.0'

import re
from word import Word, Syllable
from docopt import docopt

class SyllabDivisor:
    """Divisor silabico
    """

    def __init__(self):
        # Algunas reglas como el diptongo y el triptongo tratan de cuando las letras
        # no se pueden separar, dado que la forma de actuar de este programa es separando,
        # solo se usan las reglas de separacion y se excluiran las de no separacion, pues
        # al final quedaran juntas tal y como la regla lo pide.

        # El primer elemento de la tupla corresponde a la expresion regular para la regla
        # El segundo elemento de la tupla es la posicion donde se deben separar (empezando en 1)
        self.reglas = [
            # Una consonante entre dos vocales
            (r"[aeiouáéíóúü][bcdfghjklmnñpqrstvwxyz][aeiouáéíóúü]", 1),
            (r"[aeiouáéíóúü](rr|ll|ch)[aeiouáéíóúü]", 1),
            # Dos consonantes entre dos vocales
            (r"[aeiouáéíóúü]([bcdfgkpt][lr])[aeiouáéíóúü]", 1),
            (r"[aeiouáéíóúü]([bcdfghjklmnñpqrstvwxyz]{2})[aeiouáéíóúü]", 2),
            # Tres consonantes entre dos vocales
            (r"[aeiouáéíóúü]([bcdfghjklmnñpqrstvwxyz][bcdfgkpt][hlr])[aeiouáéíóúü]", 2),
            (r"[aeiouáéíóúü]([bcdfghjklmnñpqrstvwxyz]{3})[aeiouáéíóúü]", 3),
            # Cuatro consonantes entre dos vocales
            (r"[aeiouáéíóúü]([bcdfghjklmnñpqrstvwxyz]{2}[bcdfgkpt][lr])[aeiouáéíóúü]", 3),
            (r"[aeiouáéíóúü]([bcdfghjklmnñpqrstvwxyz]{4})[aeiouáéíóúü]", 2),
            # Dos vocales fuertes (aeo) deben estar en silabas diferentes
            # Una vocal fuerte y una debil acentuada deben estar en silabas diferentes
            (r"[aeoáéóíú][aeoáéóíú]", 1)
        ]

    def divide(self, word):
        """Encapsula el llamado a procesar."""
        syllables = [word.word]
        word.syllables = self.procesar(syllables)
        word.divided = True
        word.determine_accent_type()

    def partir(self, texto, punto):
        """Recibe una cadena de texto, la divide en el punto indicado y devuelve una lista
        con dos elementos (cada una de las partes)"""
        return [texto[0:punto], texto[punto:len(texto)]]

    def procesar(self, syllables):
        """Procesa las reglas con llamados recursivos entre procesar y apalanar."""
        for regla in self.reglas:
            for i in range(len(syllables)):
                result = re.search(regla[0], syllables[i])
                if not result is None:
                    syllables[i] = self.partir(syllables[i], result.span()[0]+regla[1])
                    return self.aplanar(syllables)
        return syllables

    def aplanar(self, syllables):
        """Recibe una lista con listas dentro y devuelve una lista plana (sin listas dentro)"""
        for i in range(len(syllables)):
            if type(syllables[i]) is list:
                temp = list(syllables[i])
                del syllables[i]
                temp = reversed(temp)
                for letra in temp:
                    syllables.insert(i, letra)
                break
        return self.procesar(syllables)


if __name__ == '__main__':
    ARGUMENTS = docopt(__doc__, version=__version__)
    DIVIDER = SyllabDivisor()
    if ARGUMENTS['--process'] or ARGUMENTS['-p']:
        WORD = Word(ARGUMENTS['WORD'])
        DIVIDER.divide(WORD)
        print(WORD.syllables_str())
    else:
        for file in ARGUMENTS['FILE']:
            try:
                word_list = open(file, 'r', encoding='utf8').read()
            except UnicodeDecodeError:
                word_list = open(file, 'r').read()
            word_list = word_list.split()
            word_list = [Word(word) for word in word_list]
            for word in word_list:
                DIVIDER.divide(word)
            textfile = [word.syllables_str() for word in word_list]
            textfile = '\n'.join(textfile)
            if ARGUMENTS['--override']:
                file = open(file, 'w', encoding='utf8')
            elif ARGUMENTS['OUTFILE']:
                file = open(ARGUMENTS['OUTFILE'], 'w', encoding='utf8')
            else:
                file = open('DIVIDED' + file, 'w', encoding='utf8')
            file.write(textfile)