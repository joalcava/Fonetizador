# -*- coding: utf-8 -*-

"""Usage:
    str_cleaner.py -m FILE ... [-o --override]
    str_cleaner.py FILE OUTFILE
    str_cleaner.py FILE [-o --override]
    str_cleaner.py (-h | --help)
    str_cleaner.py (-v | --version)

Limpia una cadena de texto, removiendo todo caracter no alfanumerico, espacios
y saltos de linea repetidos.

Arguments:
    FILE        Archivo de entrada
    FILE ...    Multiples archivos de entrada
    OUTFILE     Archivo de salida

Options:
    -h --help       Muestra esta pantalla.
    -v --version    Muestra la version.
    -o --override   Sobreescribe el archivo(s) de entrada.
    -m              Indica que se van a procesar multiples archivos.
"""

__version__ = '0.1.2'

from docopt import docopt

# TODO: Opcion de sobreescribir archivo

class StrCleaner():
    """
    Limpia una cadena de caracteres de forma que solamente quedan en ella caracteres
    alfanumericos o elimina solamente ciertos caracteres a preferencia del usuario.
    """

    def __init__(self, uri='', text='',
                 tokens=',;.:-_{[^}]`´+*~¿¡\'?\\=)(/&%$#"!°@|¬<>'):
        self.uri = uri
        self.text = text
        self.vocab = []
        self.tokens = tokens

    def load_file(self):
        """
        Intenta cargar en memoria el archivo de texto que se indico en el constructor
        """
        try:
            self.text = open(self.uri, 'r', encoding='utf8').read()
        except UnicodeDecodeError:
            self.text = open(self.uri, 'r').read()
        except FileNotFoundError:
            print('No se encuentra', self.uri)
            exit()

    def clean(self):
        """
        Elimina de self.text cualquier caracter que no sea alfanumerico
        exceptuando los acentuados y saltos de linea, tambien elimina espacios repetidos
        """
        if self.text:
            self.text = self.text.strip()
            self.text = self.text.lower()
            for i in range(len(self.text)):
                code = ord(self.text[i])
                if not((code >= 48 and code <= 57) or #[0-9]
                       (code >= 97 and code <= 122) or #[a-z]
                       (code in [225, 233, 237, 243, 250, 252, 32, 241, 10])): #[á é í ó ú ü ñ \n]
                    self.text = self.text[0:i] + ' ' + self.text[i+1:len(self.text)]
            # Dividir por saltos de linea
            temptext = self.text.splitlines()
            # Dividir por espacios
            temptext = [line.split() for line in temptext]
            # Volver a unir todo
            temptext = [' '.join(line) for line in temptext]
            while '' in temptext:
                temptext.remove('')
            temptext = '\n'.join(temptext)
            self.text = temptext
        else:
            raise Exception('No se cargo ningun texto para procesar')

    def generate_vocabulary(self, name=None):
        """
        Genera un archivo con el diccionario de palabras o vocabulario correspondiente al texto
        que se proceso. NOTA: Usar unicamente despues de haber limpiado el texto, no antes.
        Args:
            name (String) : Nombre del archivo en donde se va a guardar el vocabulario, por
                            defecto es None, con lo que se usa el nombre del archivo original
                            antecedido por la palabra VOCAB
        Return:
            void
        """
        words = self.text.split()
        for word in words:
            if not word in self.vocab:
                self.vocab.append(word)
        self.vocab.sort()
        self.vocab = '\n'.join(self.vocab)
        if name is None:
            file = open('VOCAB'+self.uri, 'w', encoding='utf8')
        else: file = open(name, 'w', encoding='utf8')
        file.write(self.vocab)

    def clean_by_tokens(self):
        """
        Elimina de self.text solo los caracteres que se hayan incluido en la lista self.tokens.
        """
        if self.text:
            self.text = self.text.strip()
            self.text = self.text.lower()
            for i in self.tokens:
                self.text = self.text.replace(i, '')
        else:
            raise Exception('No se cargo ningun archivo de texto')

    def save(self, name=None, override=False):
        """
        Crea un archivo con el contendio de self.text y lo nombra como el archivo original
        agregandole la palabra CLEANED al principio. CUIDADO: Usar esta este metodo sin
        haber indicado un archivo de entrada creara un fichero sin extension llamado CLEANED
        """
        if override:
            file = open(self.uri, 'w', encoding='utf8')
        else:
            if name:
                file = open(name, 'w', encoding='utf8')
            else:
                file = open('CLEANED'+self.uri, 'w', encoding='utf8')
        file.write(self.text)


if __name__ == '__main__':
    """Ejemplo de como usar la clase StrCleaner llamandola desde fuera:

    Enviando texto desde el constructor:
        cleaner = StrCleaner(text="  Jo¡?=)(/&%$#\"!se  \\ AleJAndro / Card..ona!")
        cleaner.Clean()
        print(cleaner.text)

    Procesando un archivo:
        cleaner = StrCleaner(uri='sample3.txt')
        cleaner.LoadFile()
        cleaner.Clean()
        cleaner.Save()
    """

    ARGUMENTS = docopt(__doc__, version=__version__)

    for _file in ARGUMENTS['FILE']:
        cleaner = StrCleaner(uri=_file)
        cleaner.load_file()
        cleaner.clean()
        cleaner.generate_vocabulary()
        cleaner.save(name=ARGUMENTS['OUTFILE'], override=ARGUMENTS['--override'])
