# -*- coding: utf-8 -*-

from enum import Enum

"""
Palabra
"""

class AccentType(Enum):
    """Enumera los tipos de acento posibles para una palabra
    """
    nodispoible = 0
    aguda = 1
    grave = 2
    esdrujula = 3
    sobreesdrujula = 4

class Word:
    """
    Representa una palabra, contiene sus letras, silabas y fonemas
    """

    def __init__(self, word):
        self.word = word
        self.__index = 0

        # Las letras son iterables desde la clase.
        self.letters = [Letter(letter) for letter in self.word]

        # Las silabas y los fonemas no se pueden acceder como iterables
        # de la clase, solo como propiedades.
        self.syllables = [Syllable(str(i)) for i in self.word]
        self.phonemes = []

        # Sirven para determinar si una palabra ya fue fonetizada o dividida silabicamente
        self.divided = False
        self.phonetized = False

        # El tipo de acento no debe calcularse hasta que se haya realizado
        # la division silabica de la palabra.
        # Por defecto la clase SyllabDivisor llama al metodo determine_accent_type()
        self.accenttype = AccentType.nodispoible

    def determine_accent_type(self):
        """
        Determina el tipo de acento de la palabra.
        Por su acento una palabra puede ser: aguda, grave, esdrujula o sobreesdrujula
        ADVERTENCIA: Solo se puede decidir el tipo si la palabra ha sido dividida silabicamente
                     con anterioridad y en el caso de llevar tilde esta este situada correctamente.
        """
        accents = 'áéíóú'
        if self.divided:
            for letter in accents:
                if letter in self.word:
                    try:
                        if letter in self.syllables[-1]:
                            self.accenttype = AccentType.aguda
                            return
                        if letter in self.syllables[-2]:
                            self.accenttype = AccentType.grave
                            return
                        if letter in self.syllables[-3]:
                            self.accenttype = AccentType.esdrujula
                            return
                        if letter in self.syllables[-4]:
                            self.accenttype = AccentType.sobreesdrujula
                            return
                    except IndexError:
                        continue
            if self.syllables[-1][-1] in 'aeiousn':
                self.accenttype = AccentType.grave
            else:
                self.accenttype = AccentType.aguda
        else:
            print('*** Debe dividirse silabicamente antes de determinar el tipo de acento ***')

    def syllables_str(self):
        """Devuelve una respresentacion string de la lista de silabas."""
        syllables = [str(syl) for syl in self.syllables]
        return '-'.join(syllables)

    def phonemes_str(self):
        """Devuelve una representacion string de la lista de fonemas."""
        return ' '.join(self.phonemes)

    def __iter__(self):
        return self

    def __next__(self):
        if self.__index == len(self.letters):
            raise StopIteration
        self.__index = self.__index + 1
        return self.letters[self.__index - 1]

    def __getitem__(self, index):
        return self.letters[index]

    def __len__(self):
        return len(self.letters)

    def __str__(self):
        return self.word

    def __repr__(self):
        return self.word


class Letter:
    """
    Representa una letra
    """

    _vocales = 'aáeéiíoóuúü'
    _consonantes = 'bcdfghjklmnñpqrstvwxyz'
    _vocales_fuertes = 'aeoáéóúí'
    _vocales_debiles = 'iuü'
    _vocalesacentuadas = 'áéíóú'

    def __init__(self, letter):
        self._letter = letter

    def esvocal(self):
        try:
            return True if self._letter in self._vocales else False
        except AttributeError:
            return True if self._syllable in self._vocales else False

    def esvocalacentuada(self):
        try:
            return True if self._letter in self._vocalesacentuadas else False
        except AttributeError:
            return True if self._syllable in self._vocalesacentuadas else False

    def esvocalfuerte(self):
        try:
            return True if self._letter in self._vocales_fuertes else False
        except AttributeError:
            if self._syllable[-1] in self._vocales_fuertes: return True
            return False

    def esvocaldebil(self):
        try:
            return True if self.__leter in self._vocales_debiles else False
        except AttributeError:
            if self._syllable[-1] in self._vocales_debiles: return True
            return False

    def esconsonante(self):
        try:
            return True if self._letter in self._consonantes else False
        except AttributeError:
            return True if self._syllable in self._consonantes else False

    def __add__(self, other):
        """La suma de dos letras da como resultado una silaba."""
        return Syllable(self._letter + other._letter)

    def __str__(self):
        return self._letter

    def __repr__(self):
        return self._letter


class Syllable(Letter):
    """
    Representa una silaba
    """

    __digrafos = ('pr', 'br', 'tr', 'dr', 'cr', 'kr', 'gr', 'fr', \
                 'pl', 'bl', 'cl', 'kl', 'gl', 'fl', 'ch', 'll', 'rr')

    def __init__(self, syllab):
        self._syllable = syllab

    def esdigrafo(self):
        return True if self._syllable in self.__digrafos else False

    def vocalesalinicio(self):
        cont = 0
        for i in range(len(self._syllable)):
            if self._syllable[i] in self._vocales: cont += 1
            else: break
        return cont

    def vocalesalfinal(self):
        cont = 0
        for i in range(len(self._syllable)-1, -1, -1):
            if self._syllable[i] in self._vocales: cont += 1
            else: break
        return cont

    def consonantesalfinal(self):
        cont = 0
        for i in range(len(self._syllable)-1, -1, -1):
            if self._syllable[i] in self._consonantes: cont += 1
            else: break
        return cont

    def consonantesalinicio(self):
        cont = 0
        for i in range(len(self._syllable)):
            if self._syllable[i] in self._consonantes: cont += 1
            else: break
        return cont

    def __add__(self, other):
        return Syllable(self._syllable + other._syllable)

    def __str__(self):
        return self._syllable

    def __repr__(self):
        return self._syllable
